import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Alert, Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";

import {registerUser} from "../../store/actions/users";
import FormElement from "../../components/UI/Form/FormElement";

class Register extends Component {
  state = {
    username: '',
    password: '',
    displayName: '',
    phone: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.registerUser(this.state);
  };

  fieldHasError = fieldName => {
    return this.props.error && this.props.error.errors[fieldName];
  };

  render() {
    let error = null;
    if (this.props.error) {
      error = <Alert bsStyle="danger">Some kind of error happened!</Alert>
    }

    return (
      <Fragment>
        <PageHeader>Register new user</PageHeader>
        {error}
        <Form horizontal onSubmit={this.submitFormHandler}>

          <FormElement
            propertyName="username"
            title="Username"
            placeholder="Enter username"
            autoComplete="new-username"
            type="text"
            value={this.state.username}
            changeHandler={this.inputChangeHandler}
            error={this.fieldHasError('username') && this.props.error.errors.username.message}
          />

          <FormElement
            propertyName="password"
            title="Password"
            placeholder="Enter password"
            autoComplete="new-password"
            type="password"
            value={this.state.password}
            changeHandler={this.inputChangeHandler}
            error={this.fieldHasError('password') && this.props.error.errors.password.message}
          />

          <FormElement
            propertyName="displayName"
            title="displayName"
            placeholder="Enter displayed name"
            autoComplete="new-displayName"
            type="text"
            value={this.state.displayName}
            changeHandler={this.inputChangeHandler}
            error={this.fieldHasError('displayName') && this.props.error.errors.displayName.message}
          />

          <FormElement
            propertyName="phone"
            title="phone"
            placeholder="Enter phone"
            autoComplete="new-phone"
            type="text"
            value={this.state.phone}
            changeHandler={this.inputChangeHandler}
            error={this.fieldHasError('phone') && this.props.error.errors.phone.message}
          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button
                bsStyle="primary"
                type="submit"
              >Register</Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);