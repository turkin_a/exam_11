import React, { Component } from 'react';
import {connect} from "react-redux";
import {Button, Image, Panel} from "react-bootstrap";

import notFound from '../../../assets/images/not_found.png';
import config from "../../../config";
import {getProductItem, removeProduct} from "../../../store/actions/products";

class ProductItem extends Component {
  componentDidMount() {
    this.props.onGetProductItem(this.props.match.params.id);
  }

  render() {
    const item = this.props.item;
    let image = notFound;

    if (item.image) {
      image = config.apiUrl + '/uploads/' + this.props.item.image;
    }

    let removeBtn = null;
    const currentUserId = this.props.user ? this.props.user._id : null;
    const sellerUserId = item.seller ? item.seller._id : null;

    if (currentUserId && currentUserId === sellerUserId) {
      removeBtn = (
        <Button bsStyle="primary" className="pull-left"
                onClick={() => this.props.onRemoveItem(item._id)}>
          Remove product
        </Button>
      );
    }

    return (
      <Panel>
        <Panel.Body>
          <Image
            style={{width: '100%'}}
            src={image}
            thumbnail
          />
          <p><strong>{item.title}</strong> Category: {item.category ? item.category.title : null}</p>
          <p><i>{item.description}</i></p>
          <p>Cost: {item.price} USD</p>
          <p>Phone: {item.seller ? item.seller.phone + ' ' + item.seller.displayName : null}</p>
          {removeBtn}
        </Panel.Body>
      </Panel>
    );
  }
}

const mapStateToProps = state => ({
  item: state.products.currentProduct,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onGetProductItem: (id) => dispatch(getProductItem(id)),
  onRemoveItem: (id) => dispatch(removeProduct(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductItem);