import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Grid, PageHeader, Row} from "react-bootstrap";
import {Link} from "react-router-dom";

import {changeCategory, fetchCategories, fetchProducts} from "../../store/actions/products";
import ProductListItem from "../../components/ProductListItem/ProductListItem";

class Products extends Component {
  componentDidMount() {
    this.props.onFetchCategories();
    this.props.onFetchProducts();
  }

  render() {
    let newProductBtn = null;

    if (this.props.user) {
      newProductBtn = (
        <Link to="/products/new">
          <Button bsStyle="primary" className="pull-right">
            Add product
          </Button>
        </Link>
      );
    }

    return (
      <Fragment>
        <PageHeader>
          Products
          {newProductBtn}
        </PageHeader>
        <Grid>
          <Row className="show-grid">
            <Col sm={3} md={3}>
              <p><strong>
                <Link to="/products" onClick={() => this.props.onChangeCategory({title: 'All items'})}>
                  All items
                </Link>
              </strong></p>
              {this.props.categories.map(category => (
                <p key={category._id}><strong>
                  <Link to="/products" onClick={() => this.props.onChangeCategory(category)}>
                    {category.title}
                  </Link>
                </strong></p>
              ))}
            </Col>
            <Col sm={9} md={9}>
              <h2>{this.props.currentCategory}</h2>
              <Row className="show-grid" style={{display: 'flex', flexWrap: 'wrap'}}>
                {this.props.products.map(product => (
                  <ProductListItem
                    key={product._id}
                    id={product._id}
                    title={product.title}
                    price={product.price}
                    image={product.image}
                  />
                ))}
              </Row>
            </Col>
          </Row>
        </Grid>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    categories: state.products.categories,
    currentCategory: state.products.currentCategory,
    products: state.products.products
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchCategories: () => dispatch(fetchCategories()),
    onFetchProducts: () => dispatch(fetchProducts()),
    onChangeCategory: (cat) => dispatch(changeCategory(cat))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);