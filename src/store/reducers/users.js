import * as actionTypes from "../actions/actionTypes";

const initialState = {
  registerError: null,
  loginError: null,
  user: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.REGISTER_USER_SUCCESS:
      return {...state, registerError: null};
    case actionTypes.REGISTER_USER_FAILURE:
      return {...state, registerError: action.error};
    case actionTypes.LOGIN_USER_SUCCESS:
      return {...state, user: action.user, loginError: null};
    case actionTypes.LOGIN_USER_FAILURE:
      return {...state, loginError: action.error};
    case actionTypes.LOGOUT_USER:
      return {...state, user: null};
    default:
      return state;
  }
};

export default reducer;