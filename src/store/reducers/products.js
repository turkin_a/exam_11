import * as actionTypes from '../actions/actionTypes';

const initialState = {
  categories: [],
  currentCategory: 'All items',
  products: [],
  currentProduct: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_CATEGORIES_SUCCESS:
      return {...state, categories: action.categories};
    case actionTypes.FETCH_PRODUCTS_SUCCESS:
      return {...state, products: action.products, currentProduct: {}};
    case actionTypes.CHANGE_CATEGORY_SUCCESS:
      return {...state, currentCategory: action.title};
    case actionTypes.GET_PRODUCT_ITEM_SUCCESS:
      return {...state, currentProduct: action.item};
    default:
      return state;
  }
};

export default reducer;