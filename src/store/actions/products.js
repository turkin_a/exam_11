import axios from '../../axios-api';
import {push} from 'react-router-redux';
import * as actionTypes from './actionTypes';

const fetchCategoriesSuccess = categories => {
  return {type: actionTypes.FETCH_CATEGORIES_SUCCESS, categories};
};

const fetchProductsSuccess = products => {
  return {type: actionTypes.FETCH_PRODUCTS_SUCCESS, products};
};

const changeCategorySuccess = title => {
  return {type: actionTypes.CHANGE_CATEGORY_SUCCESS, title};
};

const getProductItemSuccess = item => {
  return {type: actionTypes.GET_PRODUCT_ITEM_SUCCESS, item};
};

const createProductSuccess = () => {
  return {type: actionTypes.CREATE_PRODUCT_SUCCESS};
};

export const fetchCategories = () => {
  return dispatch => {
    axios.get('/categories').then(
      response => dispatch(fetchCategoriesSuccess(response.data)),
      error => console.log(error)
    )
  };
};

export const fetchProducts = catId => {
  return dispatch => {
    let url = '/products';
    if (catId) url += `?category=${catId}`;
    axios.get(url).then(
      response => dispatch(fetchProductsSuccess(response.data)),
      error => console.log(error)
    )
  };
};

export const changeCategory = cat => {
  return dispatch => {
    dispatch(fetchProducts(cat._id));
    dispatch(changeCategorySuccess(cat.title));
  };
};

export const getProductItem = id => {
  return dispatch => {
    axios.get(`/products/${id}`).then(
      response => dispatch(getProductItemSuccess(response.data)),
      error => console.log(error)
    )
  }
};

export const createProduct = productData => {
  return dispatch => {
    axios.post('/products', productData).then(
      response => dispatch(createProductSuccess()),
      error => console.log(error)
    )
  };
};

export const removeProduct = id => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const headers = {'Token': token};
    axios.delete('/products/' + id, {headers}).then(
      response => {
        console.log(response);
        dispatch(push('/'));
      },
      error => {
        console.log(error);
        dispatch(push('/'));
      }
    )
  };
};