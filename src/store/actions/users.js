import axios from '../../axios-api';
import {push} from 'react-router-redux';
import * as actionTypes from "./actionTypes";

const registerUserSuccess = () => {
  return {type: actionTypes.REGISTER_USER_SUCCESS};
};

const registerUserFailure = error => {
  return {type: actionTypes.REGISTER_USER_FAILURE, error};
};

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      response => {
        dispatch(registerUserSuccess());
        dispatch(push('/'));
      },
      error => {
        dispatch(registerUserFailure(error.response.data));
      }
    )
  };
};

const loginUserSuccess = user => {
  return {type: actionTypes.LOGIN_USER_SUCCESS, user};
};

const loginUserFailure = error => {
  return {type: actionTypes.LOGIN_USER_FAILURE, error};
};

export const loginUser = userData => {
  return dispatch => {
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data.user));
        dispatch(push('/'));
      },
      error => {
        const errorObj = error.response ? error.response.data : {error: 'No internet connection'};
        dispatch(loginUserFailure(errorObj));
      }
    )
  }
};

export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const headers = {'Token': token};
    axios.delete('/users/sessions', {headers}).then(
      response => {
        dispatch({type: actionTypes.LOGOUT_USER});
        dispatch(push('/'));
      },
      error => {
        console.log('Logout error');
      }
    )
  }
};