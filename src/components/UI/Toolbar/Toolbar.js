import React, {Fragment} from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {connect} from "react-redux";

import {logoutUser} from "../../../store/actions/users";

const Toolbar = props => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
        <LinkContainer to="/" exact><a>Flea market</a></LinkContainer>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav pullRight>
        {props.user ?
          <Fragment>
            <NavItem disabled>Hello, {props.user.displayName}!</NavItem>
            <NavItem onClick={props.logoutUser}>Logout</NavItem>
          </Fragment>
          :
          <Fragment>
            <LinkContainer to="/register" exact>
              <NavItem>Sign up</NavItem>
            </LinkContainer>
            <LinkContainer to="/login" exact>
              <NavItem>Login</NavItem>
            </LinkContainer>
          </Fragment>
        }
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default connect(null, mapDispatchToProps)(Toolbar);