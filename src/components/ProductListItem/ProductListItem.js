import React from 'react';
import {Col, Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import notFound from '../../assets/images/not_found.png';
import config from "../../config";

const ProductListItem = props => {
  let image = notFound;

  if (props.image) {
    image = config.apiUrl + '/uploads/' + props.image;
  }

  return (
    <Col xs={6} md={4} lg={3}>
      <Panel>
        <Panel.Body>
          <Link to={'/products/' + props.id}>
            <Image
              style={{width: '100%'}}
              src={image}
              thumbnail
            />
          </Link>
          <Link to={'/products/' + props.id}>
            <strong>{props.title}</strong>
          </Link>
          <p><strong>{props.price} USD</strong></p>
        </Panel.Body>
      </Panel>
    </Col>
  );
};

ProductListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  image: PropTypes.string
};

export default ProductListItem;